package org.junit.runners;

import static java.lang.String.format;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * 
 */
public class Parameterized extends Suite {
	/**
	 * Annotation for a method which provides parameters to be injected into the
	 * test class constructor by <code>Parameterized</code>
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface Parameters {
		String value();
	}

	private class TestClassRunnerForParameters extends BlockJUnit4ClassRunner {
		private final int fParameterSetNumber;

		private final Object[] fParameters;

		private TestClassRunnerForParameters(Class<?> type, Object[] parameters, int i)
				throws InitializationError {
			super(type);
			fParameters = parameters;
			fParameterSetNumber = i;
		}

		@Override
		public Object createTest() throws Exception {
			return getTestClass().getOnlyConstructor().newInstance(fParameters);
		}

		@Override
		protected String getName() {
			return String.format("[%s]", fParameterSetNumber);
		}

		@Override
		protected String testName(final FrameworkMethod method) {
			return String.format("%s[%s]", method.getName(),
					fParameterSetNumber);
		}

		@Override
		protected void validateConstructor(List<Throwable> errors) {
			validateOnlyOneConstructor(errors);
		}

		@Override
		protected Statement classBlock(RunNotifier notifier) {
			return childrenInvoker(notifier);
		}

		//@Override
		protected Annotation[] getRunnerAnnotations() {
			return new Annotation[0];
		}
	}

	private final ArrayList<Runner> runners = new ArrayList<Runner>();

	/**
	 * Only called reflectively. Do not use programmatically.
	 */
	public Parameterized(Class<?> klass) throws Throwable {
		super(klass, Collections.<Runner> emptyList());
		Collection<Iterable<Object[]>> allParameters = getAllParameters();
		createRunnersForParameters(allParameters);
	}

	@Override
	protected List<Runner> getChildren() {
		return runners;
	}

	@SuppressWarnings("unchecked")
	private Collection<Iterable<Object[]>> getAllParameters() throws Throwable {
		
		Collection<Iterable<Object[]>> result = new ArrayList<Iterable<Object[]>>();
		
		List<FrameworkMethod> methods = getParametersMethods();
		
		for (FrameworkMethod method: methods){
			
			Object parameters = method.invokeExplosively(null);
			if (parameters instanceof Iterable)
				result.add((Iterable)parameters);
			
			else {
				String className = getTestClass().getName();
				String methodName = method.getName();
				throw parametersMethodReturnedWrongType(className,methodName);
			}
		}
		return result;
	}

	private List<FrameworkMethod> getParametersMethods() throws Exception {
		
		List<FrameworkMethod> result = new ArrayList<FrameworkMethod>();

		List<FrameworkMethod> methods = getTestClass().getAnnotatedMethods(Parameters.class);

		for (FrameworkMethod each : methods) {
			Integer modifiers = each.getMethod().getModifiers();
			if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers))
				result.add(each);
		}
		
		if (result.isEmpty())
			throw new Exception("No public static parameters method on class "
				+ getTestClass().getName());
		else return result;
	}

	private void createRunnersForParameters(Collection<Iterable<Object[]>> allParameters)
			throws InitializationError, Exception {

		int i = 0;
			for (Iterable<Object[]> parameters: allParameters){
				for (Object[] parametersOfSingleTest : parameters) {
					
					Class<?> _clazz = getTestClass().getJavaClass();
					try{
					TestClassRunnerForParameters runner = 
						new TestClassRunnerForParameters(_clazz, parametersOfSingleTest, i);
					runners.add(runner);
					++i;
					} catch(ClassCastException cce){
						throw parametersMethodReturnedWrongType(_clazz.getName(),"");			
					}
				}
			}
	}

	private Exception parametersMethodReturnedWrongType(
			String className, String methodName) throws Exception {
		
		String message = format("%s.%s() must return an Iterable of arrays.",
				className, methodName);
		return new Exception(message);
	}
}
