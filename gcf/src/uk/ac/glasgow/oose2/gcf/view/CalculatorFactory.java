package uk.ac.glasgow.oose2.gcf.view;

import uk.ac.glasgow.oose2.gcf.model.BasicCalculator;
import uk.ac.glasgow.oose2.gcf.model.ExtendedCalculator;
import uk.ac.glasgow.oose2.gcf.model.ScientificCalculator;
import uk.ac.glasgow.oose2.gcf.view.gui.GuiBasic;
import uk.ac.glasgow.oose2.gcf.view.gui.GuiExtended;
import uk.ac.glasgow.oose2.gcf.view.gui.GuiScientific;
import uk.ac.glasgow.oose2.gcf.view.lui.LuiBasic;
import uk.ac.glasgow.oose2.gcf.view.lui.LuiExtended;
import uk.ac.glasgow.oose2.gcf.view.lui.LuiScientific;

/** 
 * Factory class for creating matching model-view pairs.  Consists of a single static method,
 * returning an interface to the view object that was manufactured.
 * 
 * This class must be modified if new models are added to the framework.
 * 
 * @author J Sventek
 * @author tws
 * @author Edvin Malinovskis
 * @version 1.1
 * 
 * Edit history:
 * 08/03/2008 - initial release
 * 16/03/2013 - added scientific model
 */
public class CalculatorFactory {
	
	public static enum CalculatorView {LINE,GRAPHICAL};
	
	public static enum CalculatorModel {BASIC,EXTENDED,SCIENTIFIC, FAULTY}
	
	/**
	 * Constructs matching model-view object pair, returning an interface to the view object.
	 * @param view string representing the type of view desired, "Line" or "Graphical"
	 * @param model string representing the type of model desired either "Basic", "Scientific" or "Extended"
	 * @return interface to the view object created
	 * @throws Exception if either the view-type or model-type are unknown
	 */	
	public static CalcUI create(String view, String model) throws Exception {
		
		//This method is a wrapper for create(CalculatorView, CalculatorModel) that converts
		//string representations of calculator view and model types into their
		//equivalent enumeration members.
		
		CalculatorView eView = CalculatorView.valueOf(view.toUpperCase());
		CalculatorModel eModel = CalculatorModel.valueOf(model.toUpperCase());
		
		if (eView==null) throw new Exception("Unknown View");
		if (eModel==null) throw new Exception("Unknown Model");
					
		return create(eView,eModel);
	}
	
	/**
	 * Constructs matching model-view object pair, returning an interface to the
	 * view object. The method guarantees to return a CalcUI instance for any
	 * combination of non-null enumeration members.
	 * 
	 * @param view
	 *            enumeration member representing the type of view desired
	 * @param model
	 *            enumeration representing the type of model desired
	 * @return interface to the view object created
	 */	
	public static CalcUI create(CalculatorView view, CalculatorModel  model) {
		//
		CalcUI ui = null;
		if (model.equals(CalculatorModel.BASIC)){
			BasicCalculator theModel = new BasicCalculator();	
			
			if (view.equals(CalculatorView.LINE))
				ui = new LuiBasic(theModel);
			else if (view.equals(CalculatorView.GRAPHICAL))
				ui = new GuiBasic(theModel);
			
		} else if (model.equals(CalculatorModel.EXTENDED)){
			ExtendedCalculator theModel = new ExtendedCalculator();	
			
			if (view.equals(CalculatorView.LINE))
				ui = new LuiExtended(theModel);
			else if (view.equals(CalculatorView.GRAPHICAL))
				ui = new GuiExtended(theModel);
			
		} else if (model.equals(CalculatorModel.SCIENTIFIC)){
			ScientificCalculator theModel = new ScientificCalculator();	
			
			if (view.equals(CalculatorView.LINE))
				ui = new LuiScientific(theModel);
			else if (view.equals(CalculatorView.GRAPHICAL))
				ui = new GuiScientific(theModel);
			
		} 
		ui.initialiseUI();
		return ui;
	}
}
