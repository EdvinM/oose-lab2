package uk.ac.glasgow.oose2.gcf.view.lui;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.ScientificCalculator;

/**
 * 
 * Line view for Scientific calculator - shows how to extend LuiBasic
 *  
 * @author Edvin Kalinovskis
 * @version 1.0
 */
public class LuiScientific extends LuiBasic {
	
	/** Gives access to the scientific calculator's full interface */
	private ScientificCalculator exCalc;

	public LuiScientific(ScientificCalculator theCalc) {
		super(theCalc);
		this.exCalc = theCalc;
		
	}
	
	/**
	 * Overrides displayError so that lines printed indicate that it comes from LuiScientific
	 */
	public void displayError(String err) {
		System.err.println("LuiScientific/" + err);
	}
	
	/**
	 * Override attemptCommand to process Scientific-specific commands
	 * Defer to super.attemptCommand() for unrecognized commands
	 */
	public void attemptCommand(String buf) throws CalculatorException {
		try {
			if (buf.equalsIgnoreCase("e"))  exCalc.e();
			else if (buf.equalsIgnoreCase("pow")) exCalc.power();
			else if (buf.equalsIgnoreCase("log")) exCalc.log();
			else if (buf.equalsIgnoreCase("exp")) exCalc.exp();
			else if (buf.equalsIgnoreCase("rad")) exCalc.rad();
			else if (buf.equalsIgnoreCase("sin")) exCalc.sin();
			else if (buf.equalsIgnoreCase("cos")) exCalc.cos();
			else if (buf.equalsIgnoreCase("tan")) exCalc.tan();
			else super.attemptCommand(buf);
		} catch (CalculatorException ex) {
			throw ex;
		}
	}
}
