package uk.ac.glasgow.oose2.gcf.view.gui;

import java.awt.GridLayout;
import java.awt.Color;

import javax.swing.JPanel;

import uk.ac.glasgow.oose2.gcf.model.ScientificCalculator;

/**
 * Graphical view for Scientific calculator
 *  
 * @author Edvin Malinovskis
 * @version 1.0
 * 
 */
public class GuiScientific extends GuiBasic {
	private static final long serialVersionUID = -5368279260811158496L;
	
	/** The calculator instance for this view, accessible via the ScientificCalculator features.*/
	private ScientificCalculator exCalc;
	
	/**
	 * Constructs a new scientific graphical view for the specified calculator model.
	 * @param theCalc the model for the view to present.
	 */ 
	public GuiScientific(ScientificCalculator theCalc) {
		//Pass the calculator to the super class (via the restricted BasicCalculator interface)
		super(theCalc);
		//Retain the extended calculator instance to access extended features.
		this.exCalc = theCalc;
	}
	
	/**
	 * Overrides super.initialiseUI() to provide extended features.
	 */
	public void initialiseUI() {
		// Create panel container for extra buttons
		JPanel jplExtra = new JPanel();			
		
		//  set layout manager for extra buttons
		jplExtra.setLayout(new GridLayout(2, 4, 2, 2));
	
		// Add extra buttons
		jplExtra.add(new CalculatorButton("Rad", Color.red));
		jplExtra.add(new CalculatorButton("Sin", Color.red));
		jplExtra.add(new CalculatorButton("Cos", Color.red));
		jplExtra.add(new CalculatorButton("Tan", Color.red));
		jplExtra.add(new CalculatorButton("E", Color.green));
		jplExtra.add(new CalculatorButton("Log", Color.red));
		jplExtra.add(new CalculatorButton("Exp", Color.red));
		jplExtra.add(new CalculatorButton("Pow", Color.red));
		
		//provide that JPanel object to the super class
		super.setExtra(jplExtra);
		
		// init the super class UI components
		super.initialiseUI();
		
	}

	/**
	 * overrides processOperation() in super class to handle Scientific-specific operations.
	 * if not an Scientific-specific operation, defer to super.processOperation to handle
	 * @param cmd command string for calculator operation
	 */
	public void processOperation(String cmd) {
		try {
			if (cmd.equalsIgnoreCase("e"))  exCalc.e();
			else if (cmd.equalsIgnoreCase("pow")) exCalc.power();
			else if (cmd.equalsIgnoreCase("log")) exCalc.log();
			else if (cmd.equalsIgnoreCase("exp")) exCalc.exp();
			else if (cmd.equalsIgnoreCase("rad")) exCalc.rad();
			else if (cmd.equalsIgnoreCase("sin")) exCalc.sin();
			else if (cmd.equalsIgnoreCase("cos")) exCalc.cos();
			else if (cmd.equalsIgnoreCase("tan")) exCalc.tan();
			else super.processOperation(cmd);
			
			displayTop();
		
		} catch (Exception ex) {
			displayError(ex.toString());
		}
		
	}
}
