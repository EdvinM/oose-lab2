package uk.ac.glasgow.oose2.gcf.view.gui.tests;


import static org.junit.Assert.*;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.oose2.gcf.view.CalculatorFactory;
import uk.ac.glasgow.oose2.gcf.view.gui.GuiScientific;

public class GuiScientificTest {
	
	private GuiScientific gui; 

	@Before
	public void setUp() throws Exception {
		gui = (GuiScientific)CalculatorFactory.create("GRAPHICAL", "SCIENTIFIC");
		gui.start();
	}
	
	
	
	@Test
	public void testAdd(){

		JLabel output = (JLabel)gui.getContentPane().getComponent(0);
		
		assertEquals("(empty stack)",output.getText());
		
		JPanel masterP = (JPanel)gui.getContentPane().getComponent(1);
		JPanel buttons = (JPanel)masterP.getComponent(2);
		
		JButton add = (JButton) buttons.getComponent(23);
		assertEquals("+",add.getText());
		
		JButton one = (JButton) buttons.getComponent(16);
		assertEquals("1",one.getText());
		
		JButton two = (JButton) buttons.getComponent(17);
		assertEquals("2",two.getText());
		
		JPanel control = (JPanel)masterP.getComponent(1);
		JButton enter = (JButton) control.getComponent(0);
		assertEquals("Enter",enter.getText());
		
		one.doClick();
		enter.doClick();
		two.doClick();
		enter.doClick();
		add.doClick();
		
		assertEquals("3.00",output.getText());

	}

}
