package uk.ac.glasgow.oose2.gcf.view.lui.tests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;

public abstract class LuiHarness {

	/* Gain access to the stdin/stdout process interface */
	protected PrintWriter writeToStdIn;
	protected BufferedReader readFromStdOut;
	
	private TestData td;

	/**
	 * Constructs a new instance of the test case parameterised for a single
	 * TestData tuple of input and expected output sequence.
	 * 
	 * @param td the TestData for this parameterised run.
	 */
	public LuiHarness(TestData td) {
		this.td = td;
	}
	
	@Before
	public void setUp()  throws IOException {
		
		///redirect the test's I/O so it can be written to/read via try command.
		
		PipedInputStream redirectedStdIn = new PipedInputStream();
		PipedOutputStream pos = new PipedOutputStream(redirectedStdIn);
		writeToStdIn = new PrintWriter(pos);		
		System.setIn(redirectedStdIn);
		
		PipedInputStream pis = new PipedInputStream();
		PrintStream redirectedStdOut = new PrintStream(new PipedOutputStream(pis));
		readFromStdOut = new BufferedReader(new InputStreamReader(pis));
		System.setOut(redirectedStdOut);
		
	}
	
	/**
	 * Utility interaction method for testing command sequences
	 * 
	 * @param command
	 *            a sequences of commands to make available for reading on stdin
	 * @param expected
	 *            a sequence of lines that should be printed on stdout
	 * @throws IOException
	 *             if an error occurs while interacting with LuiBasic via
	 *             redirected stdin and stdout.
	 */
	@Test
	public final void testCommandSequence() throws IOException {
		writeToStdIn.println(td.input);
		writeToStdIn.flush();

		for (String expect : td.expected) {
			String actual = readFromStdOut.readLine();
			assertEquals("Command ["+td.input+"]", expect, actual);
		}
	}
	
	static class TestData {
		
		public String input;
		public String[] expected;
		
		public TestData(String input, String... expected){
			this.input = input;
			this.expected = expected;
		}
	}
}
