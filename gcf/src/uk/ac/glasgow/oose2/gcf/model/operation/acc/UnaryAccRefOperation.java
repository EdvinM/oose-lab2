package uk.ac.glasgow.oose2.gcf.model.operation.acc;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.Accumulator;
import uk.ac.glasgow.oose2.gcf.model.CalculatorException;

public abstract class UnaryAccRefOperation extends AccRefOperation {
	
	public UnaryAccRefOperation(Stack<Double> stack, List<Accumulator> accumulators){
		super(stack,accumulators,1);
	}
	
	@Override
	protected Double doOp(Accumulator acc, List<Double> args)throws CalculatorException {
		return doOp(acc,args.get(0));
	}
	
	protected abstract Double doOp(Accumulator acc, Double d) throws CalculatorException;
}
