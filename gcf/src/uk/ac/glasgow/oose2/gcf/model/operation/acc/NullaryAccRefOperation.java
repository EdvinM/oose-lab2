package uk.ac.glasgow.oose2.gcf.model.operation.acc;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.Accumulator;
import uk.ac.glasgow.oose2.gcf.model.CalculatorException;

public abstract class NullaryAccRefOperation extends AccRefOperation{

	
	public NullaryAccRefOperation(Stack<Double> stack, List<Accumulator> accumulators) {
		super(stack,accumulators,0);
	}
	
	@Override
	protected Double doOp(Accumulator acc, List<Double> args) throws CalculatorException{
		return doOp(acc);
	}
	
	protected abstract Double doOp(Accumulator acc) throws CalculatorException;



}
