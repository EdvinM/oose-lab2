package uk.ac.glasgow.oose2.gcf.model.operation.acc.impl;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.Accumulator;
import uk.ac.glasgow.oose2.gcf.model.operation.acc.NullaryAccRefOperation;

public class AccRecall extends NullaryAccRefOperation {

	public AccRecall(Stack<Double> stack, List<Accumulator> accumulators) {
		super(stack,accumulators);
	}

	@Override
	protected Double doOp(Accumulator acc) {
		return acc.getTotal();
	}

}
