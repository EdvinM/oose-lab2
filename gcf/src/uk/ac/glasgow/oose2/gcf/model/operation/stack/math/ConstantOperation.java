package uk.ac.glasgow.oose2.gcf.model.operation.stack.math;

import java.util.Stack;
import java.util.List;

public abstract class ConstantOperation extends MathOperation{
		
	public ConstantOperation(Stack<Double> stack){
		super(stack,0);
	}
	
	@Override
	public Double doOp(List<Double> args){
		return doOp();
	}
	
	public abstract Double doOp();
}
