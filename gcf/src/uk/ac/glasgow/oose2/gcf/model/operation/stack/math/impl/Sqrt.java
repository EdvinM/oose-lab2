package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.UnaryOperation;

public class Sqrt extends UnaryOperation {

	public Sqrt(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a) throws CalculatorException {
		if (a < 0.0)
			throw new CalculatorException("sqrt", "negative number");
		return Math.sqrt(a);
	}
}