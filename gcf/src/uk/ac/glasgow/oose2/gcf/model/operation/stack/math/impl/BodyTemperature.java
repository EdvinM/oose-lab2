package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.operation.Operation;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.ConstantOperation;

public class BodyTemperature extends ConstantOperation implements
		Operation<Double> {

	public BodyTemperature(Stack<Double> stack) {
		super(stack);
	}

	@Override
	public Double doOp() {return 98.6;}
}
