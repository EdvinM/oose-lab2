package uk.ac.glasgow.oose2.gcf.model.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.ScientificCalculator;

/**
 * Tests for the Scientific model
 * 
 * @author Edvin Malinovskis
 *
 */
public class ScientificCalculatorTest extends BasicCalculatorTest{

    /** Provides an interface to the scientific features of the calculator under test */
    private ScientificCalculator sciCalc;

    private List<Double> angles;

    @Before
    @Override
    public void setUp() throws Exception {
        sciCalc = new ScientificCalculator();
        //Make sure that the basic functionality of the scientific calculator hasn't been broken.
        theCalc = sciCalc;

        angles = new ArrayList<Double>();
        for (int i = -360; i < 360; i+=45) {
            angles.add((double) i);
        }
    }

    @Test
    public final void testE() throws CalculatorException{
        sciCalc.e();
        Double result = sciCalc.result();

        assertEquals(Math.E, result, epsilon);
    }

    @Test(expected = CalculatorException.class)
    public final void testPowException() throws CalculatorException {
        sciCalc.power();
    }

    @Test
    public final void testPosPowNeg() throws CalculatorException{
        // odd
        sciCalc.enter(-2.0);
        sciCalc.enter(5.0);
        sciCalc.power();
        assertEquals(0.04, sciCalc.result(), epsilon);

        // even
        sciCalc.enter(-3.0);
        sciCalc.enter(5.0);
        sciCalc.power();
        assertEquals(0.008, sciCalc.result(), epsilon);
    }

    @Test
    public final void testNegPowPos() throws CalculatorException{
        // odd
        sciCalc.enter(5.0);
        sciCalc.enter(-2.0);
        sciCalc.power();
        assertEquals(-32, sciCalc.result(), epsilon);

        // even
        sciCalc.enter(4.0);
        sciCalc.enter(-2.0);
        sciCalc.power();
        assertEquals(16, sciCalc.result(), epsilon);
    }

    @Test
    public final void testNegPowNeg() throws CalculatorException{
        // odd^odd
        sciCalc.enter(-3.0);
        sciCalc.enter(-5.0);
        sciCalc.power();
        assertEquals(-0.008, sciCalc.result(), epsilon);

        // odd^even
        sciCalc.enter(-2.0);
        sciCalc.enter(-5.0);
        sciCalc.power();
        assertEquals(0.04, sciCalc.result(), epsilon);

        // even^odd
        sciCalc.enter(-3.0);
        sciCalc.enter(-4.0);
        sciCalc.power();
        assertEquals(-0.015625, sciCalc.result(), epsilon);

        // even^even
        sciCalc.enter(-2.0);
        sciCalc.enter(-4.0);
        sciCalc.power();
        assertEquals(0.0625, sciCalc.result(), epsilon);
    }

    @Test
    public final void testPow() throws CalculatorException{
        sciCalc.enter(2.0);
        sciCalc.enter(5.0);
        sciCalc.power();
        assertEquals(25, sciCalc.result(), epsilon);

        sciCalc.enter(5.0);
        sciCalc.enter(2.0);
        sciCalc.power();
        assertEquals(32, sciCalc.result(), epsilon);

        // ^ 1
        sciCalc.enter(1.0);
        sciCalc.enter(214.0);
        sciCalc.power();
        assertEquals(214, sciCalc.result(), epsilon);

        // ^ 0
        sciCalc.enter(0.0);
        sciCalc.enter(214.0);
        sciCalc.power();
        assertEquals(1, sciCalc.result(), epsilon);
    }

    @Test
    public final void testLog() throws CalculatorException{
        Double[][] data = {
            {0.0,Double.NEGATIVE_INFINITY},
            {1.0, 0.0},
            {2.0, 0.693147180559945},
            {5.0, 1.609437912434100},
            {10.0, 2.302585092994045}
        };
        for(Double[] row : data) {
            sciCalc.enter(row[0]);
            sciCalc.log();
            assertEquals(row[1], sciCalc.result(), epsilon);
        }
    }

    @Test
    public final void testExp() throws CalculatorException{
        sciCalc.enter(0.0);
        sciCalc.exp();
        assertEquals(1, sciCalc.result(), epsilon);

        for(int i = 1; i < 100; i*=10) {
            sciCalc.enter((double) i);
            sciCalc.exp();
            assertEquals(Math.exp(i), sciCalc.result(), epsilon);
        }
    }

    @Test
    public final void testRad() throws CalculatorException{
        for (Double angle: angles) {
            sciCalc.enter(angle);
            sciCalc.rad();
            assertEquals(Math.toRadians(angle), sciCalc.result(), epsilon);
        }
    }

    @Test
    public final void testSin() throws CalculatorException{
        for (Double angle: angles) {
            sciCalc.enter(angle);
            sciCalc.sin();
            assertEquals(Math.sin(angle), sciCalc.result(), epsilon);
        }
    }

    @Test
    public final void testCos() throws CalculatorException{
        for (Double angle: angles) {
            sciCalc.enter(angle);
            sciCalc.cos();
            assertEquals(Math.cos(angle), sciCalc.result(), epsilon);
        }
    }

    @Test
    public final void testTan() throws CalculatorException{
        for (Double angle: angles) {
            sciCalc.enter(angle);
            sciCalc.tan();
            assertEquals(Math.tan(angle), sciCalc.result(), epsilon);
        }
    }

}

