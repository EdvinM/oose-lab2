package uk.ac.glasgow.oose2.gcf.model.operation;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;

public interface Operation<S> {
	public void invoke() throws CalculatorException;
}
