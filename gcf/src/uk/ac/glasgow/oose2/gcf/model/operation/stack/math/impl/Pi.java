package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.ConstantOperation;

public class Pi extends ConstantOperation {

	public Pi(Stack<Double> stack) {
		super(stack);
	}

	@Override
	public Double doOp() {return Math.PI;}

}
