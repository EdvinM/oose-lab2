package uk.ac.glasgow.oose2.gcf.model.operation.stack.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.StackOperation;

public class Swap<S> extends StackOperation<S>{

	public Swap(Stack<S> stack) {
		super(stack);
	}

	@Override
	public void invoke() throws CalculatorException {
		S a, b;
		try {
			b = stack.pop();
			a = stack.pop();
		} catch (Exception ex) {
			throw new CalculatorException("swap", "empty stack");
		}	
		stack.push(b);
		stack.push(a);
	}

}
