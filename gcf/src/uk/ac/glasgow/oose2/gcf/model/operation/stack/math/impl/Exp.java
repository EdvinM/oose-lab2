package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.UnaryOperation;

public class Exp extends UnaryOperation {

	public Exp(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a) throws CalculatorException{
        return Math.exp(a);
	}

}


