package uk.ac.glasgow.oose2.gcf.model.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.ExtendedCalculator;

/**
 * Extends the basic tests 
 * @author tws
 *
 */
public class ExtendedCalculatorTest extends BasicCalculatorTest{

	/** Provides an interface to the extended features of the calculator under test */
	private ExtendedCalculator extCalc;
	
	@Before
	@Override
	public void setUp() throws Exception {
		extCalc = new ExtendedCalculator();
		//Make sure that the basic functionality of the extended calculator
		//hasn't been broken.
		theCalc = extCalc;
	}

	@Test
	public final void testFahr() throws CalculatorException{
		
		extCalc.enter(10.0);
		extCalc.fahr();
		Double result = extCalc.result();
		assertEquals(50.0,result,epsilon);
	}

	@Test
	public final void testCelc() throws CalculatorException{
		extCalc.enter(50.0);
		extCalc.celc();
		Double result = extCalc.result();
		assertEquals(10.0,result,epsilon);
	}
	
	@Test
	public final void testBodyTemp() throws CalculatorException{
		extCalc.bTemp();
		extCalc.celc();
		Double result = extCalc.result();
		assertEquals(37.0,result,epsilon);
		
	}

	@Test
	public final void testSwap() throws CalculatorException{
		extCalc.enter(1.0);
		extCalc.enter(2.0);
		extCalc.swap();
		Double result1 = extCalc.result();
		Double result2 = extCalc.result();
		
		assertEquals(1,result1,epsilon);
		assertEquals(2,result2,epsilon);
	}

}
