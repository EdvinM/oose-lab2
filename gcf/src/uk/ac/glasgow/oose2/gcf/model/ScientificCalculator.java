package uk.ac.glasgow.oose2.gcf.model;

import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.E;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Log;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Exp;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Sin;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Cos;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Tan;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Rad;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Power;

/**
 * Scientific extension to the basic calculator
 * 
 * @author Evin Malinovskis
 * @version 1.0
 */
public class ScientificCalculator extends BasicCalculator {

    /**
     * Constructs new Scientific Calculator
     */
    public ScientificCalculator() {
        super();
    }

    /**
     * Replace top element of the stack with its natural log
     * @throws CalculatorException
     */
    public void log() throws CalculatorException {
        new Log(stack).invoke();
    }

    /**
     * Replace top element of the stack with Euler's number e to the power of the element
     * @throws CalculatorException
     */
    public void exp() throws CalculatorException {
        new Exp(stack).invoke();
    }

    /**
     * Push the contant E onto the top element of the stack
     * @throws CalculatorException
     */
    public void e() throws CalculatorException {
        new E(stack).invoke();
    }

    /**
     * Replace the top element of the stack ( a degree value ) with the equivalent radiant value
     * @throws CalculatorException
     */
    public void rad() throws CalculatorException {
        new Rad(stack).invoke();
    }

    /**
     * Replace the top element of the stack ( a radian value ) with the equivalent trigonometric sine of an angle
     * @throws CalculatorException
     */
    public void sin() throws CalculatorException {
        new Sin(stack).invoke();
    }

    /**
     * Replace the top element of the stack ( a radian value ) with the equivalent trigonometric cosine of an angle
     * @throws CalculatorException
     */
    public void cos() throws CalculatorException {
        new Cos(stack).invoke();
    }

    /**
     * Replace the top element of the stack ( a radian value ) with the equivalent trigonometric tangent of an angle
     * @throws CalculatorException
     */
    public void tan() throws CalculatorException {
        new Tan(stack).invoke();
    }

    /**
     * Replace the top two elements of the stack with the top element raised to the power of the second
     * @throws CalculatorException
     */
	public void power() throws CalculatorException {
        new Power(stack).invoke();
    }
}
