package uk.ac.glasgow.oose2.gcf.model.operation.stack;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.operation.Operation;

public abstract class StackOperation<S> implements Operation<S>{
	
	protected Stack<S> stack;

	public StackOperation(Stack<S> stack){
		this.stack = stack;
	}
}
