package uk.ac.glasgow.oose2.gcf.model.tests.util;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomDoublePairArgumentsStream extends AbstractCollection<Object[]> {

	private Integer seed;
	private Integer size;
	
	private Double mina;
	private Double maxa;
	
	private Double minb;
	private Double maxb;	

	public RandomDoublePairArgumentsStream(
			Integer seed, Integer size, Double mina, Double maxa, Double minb, Double maxb) {
		
		this.seed = seed;
		this.size = size;
		
		this.mina = mina;
		this.maxa = maxa;
		
		this.minb = minb;
		this.maxb = maxb;
	}
	
	public RandomDoublePairArgumentsStream(Integer seed, Integer size, Double min, Double max) {
		
		this(seed,size,min,max,min,max);
	}

	@Override
	public Iterator<Object[]> iterator() {
		return new RandomDoublePairArgumentsStreamIterator();
	}

	@Override
	public int size() {
		return size;
	}

	private class RandomDoublePairArgumentsStreamIterator implements
			Iterator<Object[]> {
		
		private Integer count;
		private Iterator<Double> aIt;
		private Iterator<Double> bIt;

		private RandomDoublePairArgumentsStreamIterator(){
			this.count = 0;
			RandomDoubleStream aStream = new RandomDoubleStream(seed,size,mina,maxa);
			RandomDoubleStream bStream = new RandomDoubleStream(seed,size,minb,maxb);

			aIt = aStream.iterator();
			bIt = bStream.iterator();

		}
		
		@Override
		public boolean hasNext() {
			return count < size();
		}

		@Override
		public Object[] next() {
			if (hasNext()){
				count ++;
				return new Object[]{aIt.next(),bIt.next()};
			}
			else throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
