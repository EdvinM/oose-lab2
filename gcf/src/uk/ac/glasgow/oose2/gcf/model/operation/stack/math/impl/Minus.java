package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.BinaryOperation;

public class Minus extends BinaryOperation {

	public Minus(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a, Double b) {
		return b-a;
	}

}
