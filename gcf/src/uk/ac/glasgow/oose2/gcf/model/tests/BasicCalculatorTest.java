package uk.ac.glasgow.oose2.gcf.model.tests;

/**
 * JUnit test class for Basic Calculator
 * 
 * Tests functional aspects of the BasicCalculator class - uses the JUnit 4 framework.
 * @author tws
 */

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.oose2.gcf.model.BasicCalculator;
import uk.ac.glasgow.oose2.gcf.model.CalculatorException;

public class BasicCalculatorTest {
	
	protected static final Double epsilon = 1.0e-15;


	protected BasicCalculator theCalc;

	@Before
	public void setUp() throws Exception{
		theCalc = new BasicCalculator();
		// make sure calculator is pristine
		theCalc.clear();
	}

	/* Test pushing values onto the calculator stack. */
	
	@Test
	public void testEnter1() throws CalculatorException {
		theCalc.enter(9.0);
		Double result = theCalc.result();
		assertEquals(9.0,result,epsilon);
	}

	@Test
	public void testEnter2() throws CalculatorException {
		theCalc.enter(2.0);
		Double result = theCalc.result();
		assertEquals(2.0,result,epsilon);
	}
	
	/* Test behaviour of operations on an empty stack */

	@Test(expected = CalculatorException.class)
	public void testEmptyStackPlus() throws CalculatorException {
		theCalc.plus();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackMinus() throws CalculatorException {
		theCalc.minus();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackTimes() throws CalculatorException {
		theCalc.times();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackDivide() throws CalculatorException {
		theCalc.divide();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackSign() throws CalculatorException {
		theCalc.sign();
	}

	@Test(expected = CalculatorException.class)	
	public void testEmptyStackInvert() throws CalculatorException {
		theCalc.invert();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackSquare() throws CalculatorException {
		theCalc.square();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackSqrt() throws CalculatorException {
		theCalc.sqrt();
	}

	@Test(expected = CalculatorException.class)
	public void testEmptyStackTop() throws CalculatorException {
		theCalc.result();
	}
	
	@Test(expected = CalculatorException.class)
	public void testEmptyStackresult() throws CalculatorException {
		theCalc.result();
	}

	@Test(expected = CalculatorException.class)
	public void testMissingOperandPlus() throws CalculatorException {
		theCalc.enter(9.0);
		theCalc.plus();
	}

	@Test(expected = CalculatorException.class)
	public void testMissingOperandMinus() throws CalculatorException {
		theCalc.enter(9.0);
		theCalc.minus();
	}

	@Test(expected = CalculatorException.class)
	public void testMissingOperandTimes() throws CalculatorException {
		theCalc.enter(9.0);
		theCalc.times();
	}

	@Test(expected = CalculatorException.class)
	public void testMissingOperandDivide() throws CalculatorException {
		theCalc.enter(9.0);
		theCalc.divide();
	}

	@Test(expected = CalculatorException.class)
	public void testInvertZero() throws CalculatorException {
		theCalc.enter(0.0);
		theCalc.invert();
	}
	
	@Test(expected = CalculatorException.class)
	public void testDivideByZero() throws CalculatorException {
		theCalc.enter(2.0);
		theCalc.enter(0.0);
		theCalc.divide();
	}

	@Test(expected = CalculatorException.class)
	public void testSqrtNegativeNumber() throws CalculatorException {
		theCalc.enter(-4.0);
		theCalc.sqrt();
	}

	/* Test unary operations. */
	
	@Test
	public void testSquare() throws CalculatorException {
		theCalc.enter(2.0);
		theCalc.square();
		Double result = theCalc.result();
		assertEquals(4.0,result,epsilon);
	}

	@Test
	public void testSqrt() throws CalculatorException {
		theCalc.enter(4.0);
		theCalc.sqrt();
		Double result = theCalc.result();
		assertEquals(2.0,result,epsilon);
	}
	
	@Test
	public void testSign() throws CalculatorException {
		theCalc.enter(4.0);
		theCalc.sign();
		Double result = theCalc.result();
		assertEquals(-4.0,result,epsilon);
	}
	
	@Test
	public void testInvert() throws CalculatorException {	
		theCalc.enter(4.0);
		theCalc.invert();
		Double result = theCalc.result();
		assertEquals(0.25,result,epsilon);
	}

	/* Test binary operations */
	
	@Test
	public void testPlus() throws CalculatorException {
		theCalc.enter(2.0);
		theCalc.enter(2.0);
		theCalc.plus();
		Double result = theCalc.result();
		assertEquals(4.0,result,epsilon);
		}
	
	@Test	
	public void testMinus() throws CalculatorException {
		theCalc.enter(0.0);
		theCalc.enter(4.0);
		theCalc.minus();
		Double result = theCalc.result();
		assertEquals(-4.0,result,epsilon);
	}
	
	@Test
	public void testTimes() throws CalculatorException{
		theCalc.enter(2.0);
		theCalc.enter(2.0);
		theCalc.times();
		Double result = theCalc.result();
		assertEquals(4.0,result,epsilon);
	}
	
	@Test
	public void testDivide() throws CalculatorException{
		theCalc.enter(1.0);
		theCalc.enter(4.0);
		theCalc.divide();
		Double result = theCalc.result();
		assertEquals(0.25,result,epsilon);
	}

	/* Test Accumulator operations */
	
	@Test(expected=CalculatorException.class)
	public void testInvalidAccumulatorIndex() throws CalculatorException {
		theCalc.enter(-4.0);
		theCalc.Aclear();
	}
	
	@Test
	public void testAccumulatorStoreRecall() throws CalculatorException {	
		theCalc.enter(1.0);
		theCalc.enter(0.0);
		theCalc.Astore(); // stores 1.0 in accumulator 0
		theCalc.enter(0.0);
		theCalc.Arecall(); // recalls value of accumulator 0
		Double result = theCalc.result();
		assertEquals(1.0,result,epsilon);
	}
	
	@Test
	public void testAccumulatorPlus() throws CalculatorException {		
		theCalc.enter(2.0);
		for (int i = 0; i < 4; i++) { // add 2 to accumulator 1 four times
			theCalc.enter(2.0);
			theCalc.Aplus();
		}
		theCalc.Arecall();
		Double result = theCalc.result();
		assertEquals(8.0,result,epsilon);
	}

}
