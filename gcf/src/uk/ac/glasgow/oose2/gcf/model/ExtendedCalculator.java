package uk.ac.glasgow.oose2.gcf.model;

import uk.ac.glasgow.oose2.gcf.model.operation.stack.impl.Swap;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.BodyTemperature;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Celc;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl.Fahrenheit;

/**
 * 
 * Example extension to the basic calculator
 * 
 * @author J Sventek
 * @author tws
 * @version 1.0
 */
public class ExtendedCalculator extends BasicCalculator {
	
	/**
	 * Constructs new Extended Calculator
	 */
	public ExtendedCalculator() {
		super();
	}
	
	/** replaces the top element of the stack (a Celcius temperature value) with the equivalent Fahrenheit temperature */
	public void fahr() throws CalculatorException {new Fahrenheit(stack).invoke();}
	
	/** replaces the top element of the stack (a Fahrenheit temperature value) with the equivalent Celcius temperature */
	public void celc() throws CalculatorException {new Celc(stack).invoke();}
	
	/** swap the top two elements of the stack */
	public void swap() throws CalculatorException {new Swap<Double>(stack).invoke();}
	
	/** pushes the constant body temperature in degrees fahrenheit (98.6)onto the stack  */
	public void bTemp() throws CalculatorException {new BodyTemperature(stack).invoke();}
}