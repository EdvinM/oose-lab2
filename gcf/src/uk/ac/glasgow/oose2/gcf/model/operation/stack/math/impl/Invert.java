package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import static uk.ac.glasgow.oose2.gcf.model.BasicCalculator.SMALL_DOUBLE;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.UnaryOperation;

public class Invert extends UnaryOperation {

	public Invert(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a) throws CalculatorException{
		if (Math.abs(a) < SMALL_DOUBLE)
			throw new CalculatorException("invert", "divide by zero");
		return 1/a;
	}

}
