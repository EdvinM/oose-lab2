package uk.ac.glasgow.oose2.gcf.model.operation.acc;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.Accumulator;
import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.Operation;

public abstract class AccOperation<S> implements Operation<S> {
	protected List<Accumulator> accumulators;
	protected Stack<S> stack; 
	
	public AccOperation(Stack<S> stack, List<Accumulator> accumulators) {
		this.stack = stack;
		this.accumulators = accumulators;
	}
	
	public void doNothing(Object o){
		
	}
}