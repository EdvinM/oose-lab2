package uk.ac.glasgow.oose2.gcf.model.tests.util;

public class OperandPair<T> {
	public final T a;
	public final T b;
	
	public OperandPair(T a, T b){
		this.a = a;
		this.b = b;
	}
}
