package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.UnaryOperation;

public class Fahrenheit extends UnaryOperation{

	public Fahrenheit(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a) throws CalculatorException {
		return 1.8 * a + 32.0;
	}
}
