package uk.ac.glasgow.oose2.gcf.model.operation.stack.math;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;


public abstract class BinaryOperation extends MathOperation {
	
	public BinaryOperation(Stack<Double> stack){
		super(stack,2);
	}	
	
	@Override
	protected Double doOp(List<Double> args) throws CalculatorException{
		return doOp(args.get(0),args.get(1));
	}
	
	protected abstract Double doOp(Double a, Double b) throws CalculatorException;
}
