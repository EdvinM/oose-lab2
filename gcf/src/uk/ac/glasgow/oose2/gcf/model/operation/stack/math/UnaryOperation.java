package uk.ac.glasgow.oose2.gcf.model.operation.stack.math;

import java.util.List;
import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;

public abstract class UnaryOperation extends MathOperation {
		
	public UnaryOperation(Stack<Double> stack){
		super(stack,1);
	}
	
	@Override
	protected Double doOp(List<Double> args) throws CalculatorException{
		return doOp(args.get(0));
	}
	
	protected abstract Double doOp(Double a) throws CalculatorException;
}
