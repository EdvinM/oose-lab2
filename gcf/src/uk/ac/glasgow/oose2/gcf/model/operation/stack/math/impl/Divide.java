package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import static uk.ac.glasgow.oose2.gcf.model.BasicCalculator.SMALL_DOUBLE;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.CalculatorException;
import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.BinaryOperation;

public class Divide extends BinaryOperation {

	public Divide(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a, Double b) throws CalculatorException{
		if (Math.abs(a) < SMALL_DOUBLE)		
			throw new CalculatorException("divide", "divide by zero");
		return b/a;
	}


}
