package uk.ac.glasgow.oose2.gcf.model.tests.util;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;

public class RandomDoubleStream extends AbstractCollection<Double> {

	/****/
	private static final long serialVersionUID = -1779655300765260182L;
	
	private Integer seed;
	private Integer size;
	
	private Double min;
	private Double max;

	public RandomDoubleStream(Integer seed, Integer size, Double min, Double max) {
		this.seed = seed;
		this.size = size;
		this.max = max;
		this.min = min;
	}

	@Override
	public Iterator<Double> iterator() {
		return new RandomDoubleStreamIterator();
	}

	@Override
	public int size() {
		return size;
	}
	
	private class RandomDoubleStreamIterator implements Iterator<Double> {

		private Integer count;
		private Random random;
		
		private RandomDoubleStreamIterator(){
			this.count = 0;
			this.random = new Random(seed);
		}
		
		@Override
		public boolean hasNext() {
			return count < size();
		}

		@Override
		public Double next() {
			if (hasNext()){
				count ++;
				return random.nextDouble()*(max-min)+min;
			}
			else throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
