package uk.ac.glasgow.oose2.gcf.model.operation.stack.math.impl;

import java.util.Stack;

import uk.ac.glasgow.oose2.gcf.model.operation.stack.math.UnaryOperation;

public class Sign extends UnaryOperation {

	public Sign(Stack<Double> stack) {
		super(stack);
	}

	@Override
	protected Double doOp(Double a) {
		return -a;
	}

}
